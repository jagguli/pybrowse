#!/usr/bin/python3
"""
    ********************* VerySimpleWebBrowser ************************

    This is a Very Simple Web Browser implemented over Qt and QtWebKit.

    author: Juan Manuel Garcia <jmg.utn@gmail.com>

    *******************************************************************
"""

import os
import sys
import signal

from PyQt4 import QtCore, QtGui, QtWebKit, QtNetwork
from PyQt4.QtCore import QTimer
from PyQt4.QtGui import QApplication, QMessageBox

import logging
from urllib.parse import urlparse
default_action = "d"
search_urls = {
    "d": "http://duckduckgo.com/?q=%s&kp=-1&kl=us-en",
    "g": "https://www.google.com/search?q=%s",
    "ebayau": "http://www.ebay.com.au/sch/i.html?_nkw=%s&_armrs=1&_from=&_ipg=50",
}

class Browser(QtGui.QMainWindow):

    def __init__(self):
        """
            Initialize the browser GUI and connect the events
        """

        QtGui.QMainWindow.__init__(self)
        self.resize(800,600)
        self.centralwidget = QtGui.QWidget(self)

        self.mainLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.mainLayout.setSpacing(0)
        self.mainLayout.setMargin(1)

        self.frame = QtGui.QFrame(self.centralwidget)

        self.gridLayout = QtGui.QVBoxLayout(self.frame)
        self.gridLayout.setMargin(0)
        self.gridLayout.setSpacing(0)

        self.horizontalLayout = QtGui.QHBoxLayout()
        self.tb_url = QtGui.QLineEdit(self.frame)
        self.url_combo = QtGui.QComboBox(self)
        #self.bt_back = QtGui.QPushButton(self.frame)
        #self.bt_ahead = QtGui.QPushButton(self.frame)

        #self.bt_back.setIcon(QtGui.QIcon().fromTheme("go-previous"))
        #self.bt_ahead.setIcon(QtGui.QIcon().fromTheme("go-next"))
        #self.horizontalLayout.addWidget(self.bt_back)
        #self.horizontalLayout.addWidget(self.bt_ahead)
        self.horizontalLayout.addWidget(self.url_combo)
        self.gridLayout.addLayout(self.horizontalLayout)

        self.html = QtWebKit.QWebView()
        self.gridLayout.addWidget(self.html)
        self.mainLayout.addWidget(self.frame)
        self.setCentralWidget(self.centralwidget)

        self.connect(self.tb_url, QtCore.SIGNAL("returnPressed()"), self.browse)
        #self.connect(self.bt_back, QtCore.SIGNAL("clicked()"), self.html.back)
        #self.connect(self.bt_ahead, QtCore.SIGNAL("clicked()"), self.html.forward)

        self.default_url = "https://duckduckgo.com/"
        self.url_combo.editable = True
        self.url_combo.insertPolicy = QtGui.QComboBox.InsertAtTop
        completer = QtGui.QCompleter()
        #completer.setCompletionMode(QtGui.QCompleter.UnFilterer
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.url_combo.setCompleter(completer)
        self.url_combo.setLineEdit(self.tb_url)
        self.tb_url.setText(self.default_url)
        self.bind_keys()
        self.history = History()
        self.history.load()
        self.url_combo.addItems(self.history)

        self.browse()
        
    def focus_location_bar(self):
        self.tb_url.setFocus()
        self.tb_url.selectAll()

    def bind_keys(self):
        keys = {
            "CTRL+Q":  self.quit,
            "CTRL+L":  self.focus_location_bar,
            "ALT+Left":  self.html.back,
            "ALT+Right": self.html.forward,
        }
        for key, cmds in keys.items():
            shcut = QtGui.QShortcut(self)
            shcut.setKey(key)
            self.connect(shcut, QtCore.SIGNAL("activated()"), cmds)

    def browse(self):
        """
            Make a web browse on a specific url and show the page on the
            Webview widget.
        """
        try:

            url = self.tb_url.text() if self.tb_url.text() else self.default_url
            self.history.append(url)
            purl = urlparse(url)
            if purl.scheme not in ["http", "https", "ftp"]:
                spliturl = url.split(maxsplit=1)
                if len(spliturl) > 1:
                    action, query = spliturl
                else:
                    action, query = None, spliturl
                    
                surl = search_urls.get(action)
                if surl:
                    url = surl % query
                else:
                    url = search_urls.get(default_action) % url
            logging.debug(url)
            self.html.load(QtCore.QUrl(url))
            self.html.show()
            self.html.setFocus()
        except Exception as e:
            logging.exception("action parsing failed: %s", locals())

    def quit(self, *args):
        """Handler for the SIGINT signal."""
        sys.stderr.write('\r')
        self.history.save()
        if QMessageBox.question(None, '', "Are you sure you want to quit?",
                                QMessageBox.Yes | QMessageBox.No,
                                QMessageBox.No) == QMessageBox.Yes:
            QApplication.quit()

history_file = "~/.pybrowse/history"
history_file = os.path.expanduser(history_file)


class History(list):
    def save(self):
        with open(history_file, "a") as history:
            for url in self:
                history.write(url+"\n")

    def load(self):
        if os.path.isfile(history_file):
            with open(history_file) as history:
                for url in history:
                    self.append(url.strip())




if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG, filename=os.path.expanduser("~/.pybrowse.log"))
    from PyQt4.QtCore import pyqtRemoveInputHook
    pyqtRemoveInputHook()
    app = QtGui.QApplication(sys.argv)
    proxy = QtNetwork.QNetworkProxy()
    proxy.setType(QtNetwork.QNetworkProxy.Socks5Proxy)
    proxy.setType(QtNetwork.QNetworkProxy.HttpProxy)
    proxy.setHostName("localhost")
    proxy.setPort(8123)
    #// proxy.setUser("username")
    #// proxy.setPassword("password")
    QtNetwork.QNetworkProxy.setApplicationProxy(proxy)

    main = Browser()
    signal.signal(signal.SIGINT, main.quit)
    main.show()
    sys.exit(app.exec_())
